﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RookV2.Core;
using RookV2.Editing.Tools;
using RookV2.Input.Data;

namespace RookV2.Editing.Panels
{
	public class TilePanel : Container2D
	{
		private Texture2D tilesheet;

		private int tileSize;
		private int tilesPerRow;

		public TilePanel()
		{
			tilesheet = ContentLoader.LoadTexture("Tilesheets/Debug");
			tileSize = 32;
			tilesPerRow = tilesheet.Width / tileSize;
		}

		public TileTool TileTool { get; set; }

		public override void HandleMouse(MouseData data)
		{
			Point localPosition = data.ScreenPosition - Location;

			int tileX = localPosition.X / tileSize;
			int tileY = localPosition.Y / tileSize;

			TileTool.TileIndex = tileY * tilesPerRow + tileX;
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Draw(tilesheet, Position);
		}
	}
}
