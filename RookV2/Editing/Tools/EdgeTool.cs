﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RookV2.Input.Data;
using RookV2.Json;
using RookV2.Physics.Shapes;

namespace RookV2.Editing.Tools
{
	public class EdgeTool : EditorTool
	{
		private bool edgeActive;

		private Vector2 start;
		private Vector2 end;
		private List<Vector2> points;
		private List<EdgeCollection> collections;

		private bool gridEnabled;

		public EdgeTool()
		{
			points = new List<Vector2>();
			collections = File.Exists(Paths.Json + "Playground.json")
				? JsonUtilities.Deserialize<List<EdgeCollection>>("Playground.json")
				: new List<EdgeCollection>();
		}

		public override void HandleKeyboard(KeyboardData data)
		{
			bool controlHeld = data.KeysDown.Contains(Keys.LeftControl) || data.KeysDown.Contains(Keys.RightControl);

			if (controlHeld && data.KeysPressedThisFrame.Contains(Keys.S))
			{
				JsonUtilities.Serialize(collections, "Playground.json");
			}
		}

		public override void HandleMouse(MouseData data)
		{
			bool leftClick = data.LeftClick == ClickStates.PressedThisFrame;

			if (edgeActive)
			{
				if (data.RightClick == ClickStates.PressedThisFrame)
				{
					edgeActive = false;

					if (points.Count > 1)
					{
						collections.Add(new EdgeCollection(points.ToArray(), false));
					}

					points.Clear();
				}

				end = data.WorldPosition.ToVector2();

				// Edges can be created as a chain using a series of left clicks.
				if (leftClick)
				{
					points.Add(end);
					start = end;
				}
			}
			else if (leftClick)
			{
				start = data.WorldPosition.ToVector2();
				end = start;
				points.Add(start);
				edgeActive = true;
			}
		}

		public override void Draw(SuperBatch sb)
		{
			if (edgeActive)
			{
				for (int i = 0; i < points.Count - 1; i++)
				{
					Primitives.DrawLine(sb, points[i], points[i + 1], Color.White);
				}

				Primitives.DrawLine(sb, start, end, Color.White);
			}

			collections.ForEach(c =>
			{
				foreach (Edge edge in c.Edges)
				{
					Primitives.DrawLine(sb, edge.Start, edge.End, Color.White);
				}
			});
		}
	}
}
