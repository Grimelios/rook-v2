﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Entities.Core;
using RookV2.Input.Data;
using RookV2.Interfaces;

namespace RookV2.Editing.Tools
{
	public enum ToolTypes
	{
		Edge
	}

	public abstract class EditorTool : IDynamic, IRenderable
	{
		protected Scene Scene { get; set; }

		public virtual void RefreshScene(Scene scene)
		{
			Scene = scene;
		}

		public virtual void HandleKeyboard(KeyboardData data)
		{
		}

		public virtual void HandleMouse(MouseData data)
		{
		}

		public virtual void Update(float dt)
		{
		}

		public virtual void Draw(SuperBatch sb)
		{
		}
	}
}
