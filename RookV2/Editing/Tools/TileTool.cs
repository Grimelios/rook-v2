﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RookV2.Core;
using RookV2.Data;
using RookV2.Entities;
using RookV2.Entities.Core;
using RookV2.Input.Data;

namespace RookV2.Editing.Tools
{
	public class TileTool : EditorTool
	{
		private Texture2D tilesheet;
		private Rectangle sourceRect;
		private Vector2 tilePosition;
		private Vector2 tilemapAnchor;
		private Bounds tilemapBounds;
		private List<Entity> worldEntities;
		private List<Tilemap> tilemaps;
		private Tilemap hoveredTilemap;

		private bool tilemapMode;
		private bool tilemapInProgress;
		private bool isValid;

		private int tileSize;
		private int tilesPerRow;
		private int tileIndex;
		private int thickness;

		private float tileOpacity;

		public TileTool()
		{
			object[] values = Properties.Load("Editor.properties", new[]
			{
				new DataField("Tile.Opacity", FieldTypes.Float),
				new DataField("Tile.Thickness", FieldTypes.Integer),
			});

			tileOpacity = (float)values[0];
			thickness = (int)values[1];

			tilesheet = ContentLoader.LoadTexture("Tilesheets/Debug");
			tilemapBounds = new Bounds();
			TileSize = 32;
		}

		public int TileIndex
		{
			set
			{
				tileIndex = value;
				sourceRect.X = value % tilesPerRow * tileSize;
				sourceRect.Y = value / tilesPerRow * tileSize;
			}
		}

		public int TileSize
		{
			set
			{
				tileSize = value;
				tilesPerRow = tilesheet.Width / value;
				sourceRect = new Rectangle(0, 0, value, value);
				tilemapBounds.Width = value;
				tilemapBounds.Height = value;

				// This is called to recompute the source rectangle's location without duplicating code.
				TileIndex = tileIndex;
			}
		}

		public override void RefreshScene(Scene scene)
		{
			worldEntities = scene.GetEntities(0, EntityTypes.World);
			tilemaps = new List<Tilemap>();

			worldEntities.ForEach(e =>
			{
				Tilemap tilemap = e as Tilemap;

				if (tilemap != null)
				{
					tilemaps.Add(tilemap);
				}
			});

			base.RefreshScene(scene);
		}

		public override void HandleKeyboard(KeyboardData data)
		{
			if (data.KeysPressedThisFrame.Contains(Keys.T))
			{
				tilemapMode = !tilemapMode;
			}
		}

		public override void HandleMouse(MouseData data)
		{
			RecomputeTilePosition(data);

			if (tilemapMode)
			{
				ProcessTilemapMode(data);
			}
			else
			{
				ProcessTileMode(data);
			}
		}

		private void ProcessTilemapMode(MouseData data)
		{
			if (tilemapInProgress)
			{
				if (data.RightClick == ClickStates.PressedThisFrame)
				{
					ResetTilemap();

					return;
				}

				tilemapBounds.X = (int)MathHelper.Min(tilePosition.X, tilemapAnchor.X);
				tilemapBounds.Y = (int)MathHelper.Min(tilePosition.Y, tilemapAnchor.Y);
				tilemapBounds.Width = (int)Math.Abs(tilePosition.X - tilemapAnchor.X) + tileSize;
				tilemapBounds.Height = (int)Math.Abs(tilePosition.Y - tilemapAnchor.Y) + tileSize;
			}
			else
			{
				tilemapBounds.Position = tilePosition.ToPoint();
			}

			isValid = !tilemaps.Any(t => tilemapBounds.Overlaps(t.Bounds));

			if (isValid && data.LeftClick == ClickStates.PressedThisFrame)
			{
				if (tilemapInProgress)
				{
					Tilemap tilemap = new Tilemap(tilemapBounds, tilesheet, tileSize, tilesPerRow);

					tilemaps.Add(tilemap);
					worldEntities.Add(tilemap);
					ResetTilemap();
				}
				else
				{
					tilemapAnchor = tilePosition;
					tilemapInProgress = true;
				}
			}
		}

		private void ProcessTileMode(MouseData data)
		{
			hoveredTilemap = tilemaps.FirstOrDefault(t => t.Bounds.Contains(data.WorldPosition));
			isValid = hoveredTilemap != null;

			if (isValid && data.LeftClick == ClickStates.Held)
			{
				Point tileCoords = (tilePosition - hoveredTilemap.Position).ToPoint();
				tileCoords.X /= tileSize;
				tileCoords.Y /= tileSize;

				hoveredTilemap.Tiles[tileCoords.X, tileCoords.Y] = tileIndex;
			}
		}

		private void RecomputeTilePosition(MouseData data)
		{
			Point mousePosition = data.WorldPosition;
			tilePosition = new Vector2(mousePosition.X / tileSize, mousePosition.Y / tileSize) * tileSize;

			if (mousePosition.X < 0 && mousePosition.X % tileSize != 0)
			{
				tilePosition.X -= tileSize;
			}

			if (mousePosition.Y < 0 && mousePosition.Y % tileSize != 0)
			{
				tilePosition.Y -= tileSize;
			}
		}

		private void ResetTilemap()
		{
			tilemapInProgress = false;
			tilemapBounds = new Bounds(tilePosition.ToPoint(), tileSize, tileSize);
		}

		public override void Draw(SuperBatch sb)
		{
			Color color = isValid ? EditorColors.Base : EditorColors.Invalid;

			if (tilemapMode)
			{
				Primitives.DrawBounds(sb, tilemapBounds, color, thickness);
			}
			else
			{
				sb.Draw(tilesheet, tilePosition, sourceRect, color * tileOpacity);
			}
		}
	}
}
