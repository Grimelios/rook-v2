﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RookV2.Data;

namespace RookV2.Editing
{
	public static class EditorColors
	{
		static EditorColors()
		{
			object[] values = Properties.Load("Editor.properties", new []
			{
				new DataField("Base.Color", FieldTypes.Color),
				new DataField("Invalid.Color", FieldTypes.Color),
			});

			Base = (Color)values[0];
			Invalid = (Color)values[1];
		}

		public static Color Base { get; }
		public static Color Invalid { get; }
	}
}
