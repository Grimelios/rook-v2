﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RookV2.Data;
using RookV2.Interfaces;
using RookV2.Physics;

namespace RookV2.Editing
{
	public class EditorGrid : IRenderable
	{
		private const int Division = 4;
		private const int Step = PhysicsConstants.PixelsPerMeter / Division;

		private Color primaryColor;
		private Color secondaryColor;
		private Camera camera;

		private int rows;
		private int columns;

		public EditorGrid(Camera camera)
		{
			this.camera = camera;

			object[] values = Properties.Load("Editor.properties", new []
			{
				new DataField("Grid.Primary.Color", FieldTypes.Color),
				new DataField("Grid.Secondary.Color", FieldTypes.Color),
			});

			primaryColor = (Color)values[0];
			secondaryColor = (Color)values[1];

			rows = Resolution.WindowHeight / Step + 2;
			columns = Resolution.WindowWidth / Step + 2;
		}

		public void Draw(SuperBatch sb)
		{
			Vector2 origin = camera.VisibleArea.Position.ToVector2();
			origin.X -= origin.X % Step + (origin.X < 0 ? Step : 0);
			origin.Y -= origin.Y % Step + (origin.Y < 0 ? Step : 0);

			Vector2 start = origin;
			Vector2 end = origin + new Vector2(Resolution.WindowWidth + Step, 0);

			Point highlight = new Point((int)origin.X / Step % Division, (int)origin.Y / Step % Division);

			for (int i = 0; i < rows; i++)
			{
				Primitives.DrawLine(sb, start, end, (i + highlight.Y) % Division == 0 ? primaryColor : secondaryColor);

				start.Y += Step;
				end.Y += Step;
			}

			start = origin;
			end = origin + new Vector2(0, Resolution.WindowHeight + Step);

			for (int i = 0; i < columns; i++)
			{
				Primitives.DrawLine(sb, start, end, (i + highlight.X) % Division == 0 ? primaryColor : secondaryColor);

				start.X += Step;
				end.X += Step;
			}

			Messaging.Send(MessageTypes.Debug, $"{origin.X}, {origin.Y}");
			Messaging.Send(MessageTypes.Debug, highlight.ToString());
		}
	}
}
