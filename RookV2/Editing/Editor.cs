﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RookV2.Editing.Tools;
using RookV2.Entities.Core;
using RookV2.Input.Data;
using RookV2.Interfaces;

namespace RookV2.Editing
{
	public class Editor : IDynamic, IRenderable
	{
		private Camera camera;
		private EditorTool[] tools;
		private EditorTool activeTool;
		private EditorGrid grid;

		private bool cameraDragActive;
		private bool gridEnabled;

		public Editor(Camera camera)
		{
			this.camera = camera;

			grid = new EditorGrid(camera);
			tools = new EditorTool[]
			{
				new EdgeTool(),
				new TileTool()
			};

			activeTool = tools[1];

			Messaging.Subscribe(MessageTypes.Keyboard, (data, dt) => HandleKeyboard((KeyboardData)data));
			Messaging.Subscribe(MessageTypes.Mouse, (data, dt) => HandleMouse((MouseData)data));
		}

		public Scene Scene
		{
			set
			{
				foreach (EditorTool tool in tools)
				{
					tool.RefreshScene(value);
				}
			}
		}

		public void ToggleGrid()
		{
			gridEnabled = !gridEnabled;
		}

		private void HandleKeyboard(KeyboardData data)
		{
			if (data.KeysPressedThisFrame.Contains(Keys.G))
			{
				gridEnabled = !gridEnabled;
			}

			activeTool?.HandleKeyboard(data);
		}

		private void HandleMouse(MouseData data)
		{
			switch (data.MiddleClick)
			{
				case ClickStates.PressedThisFrame: cameraDragActive = true; break;
				case ClickStates.ReleasedThisFrame: cameraDragActive = false; break;
			}

			if (cameraDragActive)
			{
				camera.Position -= (data.ScreenPosition - data.PreviousScreenPosition).ToVector2();

				return;
			}

			activeTool?.HandleMouse(data);
		}

		public void Update(float dt)
		{
			activeTool?.Update(dt);
		}

		public void Draw(SuperBatch sb)
		{
			if (gridEnabled)
			{
				grid.Draw(sb);
			}
			
			activeTool?.Draw(sb);
		}
	}
}
