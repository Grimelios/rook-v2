﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RookV2
{
	public enum Coordinates
	{
		Screen,
		World
	}

	public class SuperBatch
	{
		private Camera camera;
		private SpriteBatch sb;
		private GraphicsDevice graphicsDevice;

		public SuperBatch(Camera camera, SpriteBatch sb, GraphicsDevice graphicsDevice)
		{
			this.sb = sb;
			this.camera = camera;
			this.graphicsDevice = graphicsDevice;
		}
		
		public void Begin(Coordinates coordinates, Effect effect = null)
		{
			Matrix transform = coordinates == Coordinates.World ? camera.Transform : Matrix.Identity;

			sb.Begin(SpriteSortMode.Deferred, null, null, null, null, effect, transform);
		}

		public void End()
		{
			sb.End();
		}

		public void Draw(Texture2D texture, Vector2 position)
		{
			sb.Draw(texture, position, null, Color.White);
		}

		public void Draw(Texture2D texture, Vector2 position, Color color)
		{
			sb.Draw(texture, position, null, color);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect)
		{
			sb.Draw(texture, position, sourceRect, Color.White);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect, Color color)
		{
			sb.Draw(texture, position, sourceRect, color);
		}

		public void Draw(Texture2D texture, Rectangle destinationRect, Color color)
		{
			sb.Draw(texture, destinationRect, color);
		}

		public void Draw(Texture2D texture, Rectangle destinationRect, Rectangle sourceRect)
		{
			sb.Draw(texture, destinationRect, sourceRect, Color.White);
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect, Color color, float rotation)
		{
			sb.Draw(texture, position, sourceRect, color, rotation, Vector2.Zero, 1, SpriteEffects.None, 0);
		}
		
		public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRect, Color color, float rotation, Vector2 origin,
			float scale, SpriteEffects effects)
		{
			sb.Draw(texture, position, sourceRect, color, rotation, origin, scale, effects, 0);
		}

		public void DrawString(SpriteFont font, string value, Vector2 position, Color color)
		{
			sb.DrawString(font, value, position, color);
		}

		public void DrawString(SpriteFont font, string value, Vector2 position, Color color, float rotation, Vector2 origin, float scale)
		{
			sb.DrawString(font, value, position, color, rotation, origin, scale, SpriteEffects.None, 0);
		}
	}
}
