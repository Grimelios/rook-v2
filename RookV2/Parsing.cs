﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2
{
	public static class Parsing
	{
		public static Color ParseColor(string value)
		{
			string[] tokens = value.Split(',');

			if (tokens.Length == 1)
			{
				int lightness = int.Parse(tokens[0]);

				return new Color(lightness, lightness, lightness);
			}

			int r = int.Parse(tokens[0]);
			int g = int.Parse(tokens[1]);
			int b = int.Parse(tokens[2]);
			int a = tokens.Length == 4 ? int.Parse(tokens[3]) : 255;

			return new Color(r, g, b, a);
		}
	}
}
