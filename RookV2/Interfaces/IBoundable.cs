﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Core;

namespace RookV2.Interfaces
{
	public interface IBoundable
	{
		Bounds Bounds { get; }
	}
}
