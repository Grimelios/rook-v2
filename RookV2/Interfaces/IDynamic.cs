﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Interfaces
{
	public interface IDynamic
	{
		void Update(float dt);
	}
}
