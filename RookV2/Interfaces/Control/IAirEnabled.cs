﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;

namespace RookV2.Interfaces.Control
{
	public interface IAirEnabled : IPositionable
	{
		Body Body { get; }

		float Acceleration { get; }
		float Deceleration { get; }
	}
}
