﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;

namespace RookV2.Interfaces.Control
{
	public interface IRunEnabled
	{
		Vector2 EdgePosition { get; set; }

		float Acceleration { get; }
		float Deceleration { get; }
		float SlideAcceleration { get; }
		float SlideDeceleration { get; }
		float MaxSpeed { get; }
		float MaxSlope { get; }
		float MaxSlideSlope { get; }
		float MaxSlideSpeed { get; }

		void SignalSlide();
		void SignalUnslide();
		void SignalPerch();
		void SignalFall();
	}
}
