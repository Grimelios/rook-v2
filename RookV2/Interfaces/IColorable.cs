﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2.Interfaces
{
	public interface IColorable
	{
		Color Color { get; set; }
	}
}
