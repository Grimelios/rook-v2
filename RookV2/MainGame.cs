﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RookV2.Animation;
using RookV2.Entities;
using RookV2.Entities.Core;
using RookV2.Input;
using RookV2.State;
using RookV2.UI;

namespace RookV2
{
	[Flags]
	public enum Alignments
	{
		Left = 1 << 0,
		Right = 1 << 1,
		Top = 1 << 2,
		Bottom = 1 << 3,
		Center = 0
	}

	public class MainGame : Game
	{
		private GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;
		private SuperBatch superBatch;
		private Camera camera;
		private GameLoop gameLoop;
		private DebugDisplay debugDisplay;
		private InputGenerator inputGenerator;

		public MainGame()
		{
			graphics = new GraphicsDeviceManager(this)
			{
				PreferredBackBufferWidth = Resolution.WindowWidth,
				PreferredBackBufferHeight = Resolution.WindowHeight
			};

			Content.RootDirectory = "Content";
			IsMouseVisible = true;
			Window.Position = new Point(180, 120);

			Messaging.Subscribe(MessageTypes.Exit, (data, dt) => Exit());
		}

		protected override void Initialize()
		{
			ContentLoader.Content = Content;
			Primitives.Initialize(GraphicsDevice);

			camera = new Camera();
			inputGenerator = new InputGenerator(camera);
			debugDisplay = new DebugDisplay
			{
				Position = new Vector2(10)
			};

			//gameLoop = new GameplayLoop();
			gameLoop = new EditorLoop();
			gameLoop.Initialize(camera);

			base.Initialize();
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			superBatch = new SuperBatch(camera, spriteBatch, GraphicsDevice);
		}

		protected override void UnloadContent()
		{
		}

		protected override void Update(GameTime gameTime)
		{
			float dt = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

			inputGenerator.Update(dt);
			gameLoop.Update(dt);
			camera.Update(dt);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);
			gameLoop.Draw(superBatch);

			superBatch.Begin(Coordinates.Screen);
			debugDisplay.Draw(superBatch);
			superBatch.End();
		}
	}
}
