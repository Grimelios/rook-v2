﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace RookV2.Json
{
	public static class JsonUtilities
	{
		public static T Deserialize<T>(string filename)
		{
			return JsonConvert.DeserializeObject<T>(File.ReadAllText(Paths.Json + filename));
		}

		public static void Serialize(object data, string filename)
		{
			File.WriteAllText(Paths.Json + filename, JsonConvert.SerializeObject(data));
		}

		public static void Write(JsonWriter writer, Vector2 value)
		{
			writer.WriteRawValue($"\"{value.X},{value.Y}\"");
		}

		public static Vector2 ParseVector2(string value)
		{
			string[] tokens = value.Split(',');

			float x = float.Parse(tokens[0]);
			float y = float.Parse(tokens[1]);

			return new Vector2(x, y);
		}
	}
}
