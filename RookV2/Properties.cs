﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Data;

namespace RookV2
{
	using PropertyMap = Dictionary<string, string>;
	using PropertyCache = Dictionary<string, Dictionary<string, string>>;

	public static class Properties
	{
		private static PropertyCache cache = new PropertyCache();

		public static object[] Load(string filename, DataField[] fields)
		{
			PropertyMap map = Load(filename);

			object[] values = new object[fields.Length];

			for (int i = 0; i < fields.Length; i++)
			{
				values[i] = ParseValue(map, fields[i]);
			}

			return values;
		}

		public static PropertyMap Load(string filename)
		{
			PropertyMap map;

			if (!cache.TryGetValue(filename, out map))
			{
				map = new PropertyMap();

				foreach (string line in File.ReadAllLines(Paths.Properties + filename))
				{
					if (line.Length == 0)
					{
						continue;
					}

					string[] tokens = line.Split('=');
					string key = tokens[0].TrimEnd();
					string value = tokens[1].TrimStart();

					map.Add(key, value);
				}

				cache.Add(filename, map);
			}

			return map;
		}

		private static object ParseValue(PropertyMap map, DataField field)
		{
			string rawValue = map[field.Name];

			switch (field.Type)
			{
				case FieldTypes.Integer: return int.Parse(rawValue);
				case FieldTypes.Float: return float.Parse(rawValue);
				case FieldTypes.String: return rawValue;
				case FieldTypes.Color: return Parsing.ParseColor(rawValue);
				case FieldTypes.Enumeration: return Enum.Parse(((EnumeratedDataField)field).EnumerationType, rawValue);
			}

			return null;
		}
	}
}
