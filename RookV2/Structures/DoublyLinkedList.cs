﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Structures
{
	public class DoublyLinkedList<T> : IEnumerable<T>
	{
		public DoublyLinkedListNode<T> Head { get; private set; }
		public DoublyLinkedListNode<T> Tail { get; private set; }

		public int Count { get; private set; }

		public void Add(T data)
		{
			DoublyLinkedListNode<T> node = new DoublyLinkedListNode<T>(data);

			if (Count == 0)
			{
				Head = node;
				Tail = node;
			}
			else
			{
				Tail.Next = node;
				node.Previous = Tail;
				Tail = node;
			}

			Count++;
		}

		public void AddRange(IEnumerable<T> range)
		{
			foreach (T item in range)
			{
				Add(item);
			}
		}

		public void Remove(DoublyLinkedListNode<T> node)
		{
			if (Count == 1)
			{
				Head = null;
				Tail = null;
			}
			else if (node == Head)
			{
				Head.Next.Previous = null;
				Head = Head.Next;
			}
			else if (node == Tail)
			{
				Tail.Previous.Next = null;
				Tail = Tail.Previous;
			}
			else
			{
				node.Previous.Next = node.Next;
				node.Next.Previous = node.Previous;
			}

			Count--;
		}

		public IEnumerator<T> GetEnumerator()
		{
			DoublyLinkedListNode<T> node = Head;

			while (node != null)
			{
				yield return node.Data;

				node = node.Next;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
