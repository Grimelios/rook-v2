﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RookV2.Input.Data;
using RookV2.Interfaces;

namespace RookV2.Input
{
	public class InputGenerator : IDynamic
	{
		private KeyboardState oldKS;
		private KeyboardState newKS;
		private MouseState oldMS;
		private MouseState newMS;
		private GamePadState oldGS;
		private GamePadState newGS;
		private Camera camera;

		// Computing world position requires using the camera's view transform. As such, it's easier to store the previous position here
		// rather than recomputing the value each frame.
		private Point previousWorldPosition;

		public InputGenerator(Camera camera)
		{
			this.camera = camera;
		}

		public void Update(float dt)
		{
			KeyboardData keyboardData = GenerateKeyboardData();
			MouseData mouseData = GenerateMouseData();
			AggregateInputData aggregateData = new AggregateInputData()
				.Add(keyboardData)
				.Add(mouseData);

			oldGS = newGS;
			newGS = GamePad.GetState(PlayerIndex.One);

			bool controllerConnected = newGS.IsConnected;

			if (controllerConnected)
			{
				Xbox360Data xbox360Data = GenerateXbox360Data();

				aggregateData.Add(xbox360Data);

				Messaging.Send(MessageTypes.Xbox360, xbox360Data, dt);
			}

			Messaging.Send(MessageTypes.Keyboard, keyboardData, dt);
			Messaging.Send(MessageTypes.Mouse, mouseData, dt);
			Messaging.Send(MessageTypes.Input, aggregateData, dt);
		}

		private KeyboardData GenerateKeyboardData()
		{
			oldKS = newKS;
			newKS = Keyboard.GetState();

			List<Keys> oldKeysDown = new List<Keys>(oldKS.GetPressedKeys());
			List<Keys> newKeysDown = new List<Keys>(newKS.GetPressedKeys());
			List<Keys> keysPressedThisFrame = newKeysDown.Except(oldKeysDown).ToList();
			List<Keys> keysReleasedThisFrame = oldKeysDown.Except(newKeysDown).ToList();

			return new KeyboardData(newKeysDown, keysPressedThisFrame, keysReleasedThisFrame);
		}

		private MouseData GenerateMouseData()
		{
			oldMS = newMS;
			newMS = Mouse.GetState();

			Point screenPosition = new Point(newMS.X, newMS.Y);
			Point previousScreenPosition = new Point(oldMS.X, oldMS.Y);
			Point worldPosition = Vector2.Transform(screenPosition.ToVector2() / Resolution.Scaling,
				Matrix.Invert(camera.Transform)).ToPoint();

			ClickStates leftClick = GetClickState(oldMS.LeftButton, newMS.LeftButton);
			ClickStates rightClick = GetClickState(oldMS.RightButton, newMS.RightButton);
			ClickStates middleClick = GetClickState(oldMS.MiddleButton, newMS.MiddleButton);
			ClickStates sideButton1 = GetClickState(oldMS.XButton1, newMS.XButton1);
			ClickStates sideButton2 = GetClickState(oldMS.XButton2, newMS.XButton2);

			MouseData data = new MouseData(screenPosition, previousScreenPosition, worldPosition, previousWorldPosition,
				leftClick, rightClick, middleClick, sideButton1, sideButton2);

			previousWorldPosition = worldPosition;

			return data;
		}

		private Xbox360Data GenerateXbox360Data()
		{
			GamePadButtons oldButtons = oldGS.Buttons;
			GamePadButtons newButtons = newGS.Buttons;

			GamePadDPad oldDPad = oldGS.DPad;
			GamePadDPad newDPad = newGS.DPad;

			ClickStates a = GetClickState(oldButtons.A, newButtons.A);
			ClickStates b = GetClickState(oldButtons.B, newButtons.B);
			ClickStates x = GetClickState(oldButtons.X, newButtons.X);
			ClickStates y = GetClickState(oldButtons.Y, newButtons.Y);
			ClickStates back = GetClickState(oldButtons.Back, newButtons.Back);
			ClickStates start = GetClickState(oldButtons.Start, newButtons.Start);
			ClickStates leftBumper = GetClickState(oldButtons.LeftShoulder, newButtons.LeftShoulder);
			ClickStates rightBumper = GetClickState(oldButtons.RightShoulder, newButtons.RightShoulder);
			ClickStates l3 = GetClickState(oldButtons.LeftStick, newButtons.LeftStick);
			ClickStates r3 = GetClickState(oldButtons.RightStick, newButtons.RightStick);
			ClickStates bigButton = GetClickState(oldButtons.BigButton, newButtons.BigButton);
			ClickStates dPadLeft = GetClickState(oldDPad.Left, newDPad.Left);
			ClickStates dPadRight = GetClickState(oldDPad.Right, newDPad.Right);
			ClickStates dPadUp = GetClickState(oldDPad.Up, newDPad.Up);
			ClickStates dPadDown = GetClickState(oldDPad.Down, newDPad.Down);

			Vector2 leftStick = newGS.ThumbSticks.Left;
			Vector2 rightStick = newGS.ThumbSticks.Right;

			float leftTrigger = newGS.Triggers.Left;
			float rightTrigger = newGS.Triggers.Right;

			return new Xbox360Data(a, b, x, y,
				back, start,
				leftBumper, rightBumper,
				l3, r3,
				bigButton,
				dPadLeft, dPadRight, dPadUp, dPadDown,
				leftStick, rightStick,
				leftTrigger, rightTrigger);
		}

		private ClickStates GetClickState(ButtonState oldState, ButtonState newState)
		{
			if (oldState == newState)
			{
				return newState == ButtonState.Pressed ? ClickStates.Held : ClickStates.Released;
			}

			return newState == ButtonState.Pressed ? ClickStates.PressedThisFrame : ClickStates.ReleasedThisFrame;
		}
	}
}
