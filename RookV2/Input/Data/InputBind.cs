﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Input.Data
{
	public class InputBind
	{
		public InputBind(InputTypes inputType, object data)
		{
			InputType = inputType;
			Data = data;
		}

		public InputTypes InputType { get; set; }

		public object Data { get; set; }
	}
}
