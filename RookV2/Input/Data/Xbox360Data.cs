﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2.Input.Data
{
	public enum Xbox360Buttons
	{
		A,
		B,
		X,
		Y,
		Back,
		Start,
		LeftBumper,
		RightBumper,
		L3,
		R3,
		BigButton,
		DPadLeft,
		DPadRight,
		DPadUp,
		DPadDown
	}

	public class Xbox360Data : InputData
	{
		public Xbox360Data(ClickStates a, ClickStates b, ClickStates x, ClickStates y,
			ClickStates back, ClickStates start,
			ClickStates leftBumper, ClickStates rightBumper,
			ClickStates l3, ClickStates r3,
			ClickStates bigButton,
			ClickStates dPadLeft, ClickStates dPadRight, ClickStates dPadUp, ClickStates dPadDown,
			Vector2 leftStick, Vector2 rightStick,
			float leftTrigger, float rightTrigger) : base(InputTypes.Xbox360)
		{
			A = a;
			B = b;
			X = x;
			Y = y;
			Back = back;
			Start = start;
			LeftBumper = leftBumper;
			RightBumper = rightBumper;
			L3 = l3;
			R3 = r3;
			BigButton = bigButton;
			DPadLeft = dPadLeft;
			DPadRight = dPadRight;
			DPadUp = dPadUp;
			DPadDown = dPadDown;
			LeftStick = leftStick;
			RightStick = rightStick;
			LeftTrigger = leftTrigger;
			RightTrigger = rightTrigger;
		}

		public ClickStates A { get; }
		public ClickStates B { get; }
		public ClickStates X { get; }
		public ClickStates Y { get; }
		public ClickStates Back { get; }
		public ClickStates Start { get; }
		public ClickStates LeftBumper { get; }
		public ClickStates RightBumper { get; }
		public ClickStates L3 { get; }
		public ClickStates R3 { get; }
		public ClickStates BigButton { get; }
		public ClickStates DPadLeft { get; }
		public ClickStates DPadRight { get; }
		public ClickStates DPadUp { get; }
		public ClickStates DPadDown { get; }

		public Vector2 LeftStick { get; }
		public Vector2 RightStick { get; }

		public float LeftTrigger { get; }
		public float RightTrigger { get; }

		public override bool Query(object data, ClickStates clickState)
		{
			switch ((Xbox360Buttons)data)
			{
				case Xbox360Buttons.A: return clickState == A;
				case Xbox360Buttons.B: return clickState == B;
				case Xbox360Buttons.X: return clickState == X;
				case Xbox360Buttons.Y: return clickState == Y;
				case Xbox360Buttons.Back: return clickState == Back;
				case Xbox360Buttons.Start: return clickState == Start;
				case Xbox360Buttons.LeftBumper: return clickState == LeftBumper;
				case Xbox360Buttons.RightBumper: return clickState == RightBumper;
				case Xbox360Buttons.L3: return clickState == L3;
				case Xbox360Buttons.R3: return clickState == R3;
				case Xbox360Buttons.BigButton: return clickState == BigButton;
				case Xbox360Buttons.DPadLeft: return clickState == DPadLeft;
				case Xbox360Buttons.DPadRight: return clickState == DPadRight;
				case Xbox360Buttons.DPadUp: return clickState == DPadUp;
				case Xbox360Buttons.DPadDown: return clickState == DPadDown;
			}

			return false;
		}
	}
}
