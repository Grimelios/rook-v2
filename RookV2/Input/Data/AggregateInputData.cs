﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Input.Data
{
	public class AggregateInputData
	{
		private InputData[] dataArray = new InputData[Functions.EnumCount<InputTypes>()];

		public AggregateInputData Add(InputData data)
		{
			dataArray[(int)data.InputType] = data;

			return this;
		}

		public InputData this[InputTypes inputType] => dataArray[(int)inputType];

		public bool Query(List<InputBind> binds, ClickStates clickState)
		{
			// For a single action, the player will usually have multiple binds for different input devices (although you could also bind
			// multiple buttons to the same action on the same device).
			return binds.Any(b => dataArray[(int)b.InputType].Query(b.Data, clickState));
		}
	}
}
