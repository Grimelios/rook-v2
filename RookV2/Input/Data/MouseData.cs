﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2.Input.Data
{
	public enum ClickStates
	{
		Held,
		Released,
		PressedThisFrame,
		ReleasedThisFrame
	}

	public enum MouseButtons
	{
		Left,
		Right,
		Middle,
		Side1,
		Side2
	}

	public class MouseData : InputData
	{
		public MouseData(Point screenPosition, Point previousScreenPosition, Point worldPosition, Point previousWorldPosition,
			ClickStates leftClick, ClickStates rightClick, ClickStates middleClick, ClickStates sideButton1, ClickStates sideButton2) :
			base(InputTypes.Mouse)
		{
			ScreenPosition = screenPosition;
			PreviousScreenPosition = previousScreenPosition;
			WorldPosition = worldPosition;
			PreviousWorldPosition = previousWorldPosition;
			LeftClick = leftClick;
			RightClick = rightClick;
			MiddleClick = middleClick;
			SideButton1 = sideButton1;
			SideButton2 = sideButton2;
		}

		public Point ScreenPosition { get; }
		public Point PreviousScreenPosition { get; }
		public Point WorldPosition { get; }
		public Point PreviousWorldPosition { get; }

		public ClickStates LeftClick { get; }
		public ClickStates RightClick { get; }
		public ClickStates MiddleClick { get; }
		public ClickStates SideButton1 { get; }
		public ClickStates SideButton2 { get; }

		public override bool Query(object data, ClickStates clickState)
		{
			// Scroll wheel is intentionally not mapped.
			switch ((MouseButtons)data)
			{
				case MouseButtons.Left: return clickState == LeftClick;
				case MouseButtons.Right: return clickState == RightClick;
				case MouseButtons.Middle: return clickState == MiddleClick;
				case MouseButtons.Side1: return clickState == SideButton1;
				case MouseButtons.Side2: return clickState == SideButton2;
			}

			return false;
		}
	}
}
