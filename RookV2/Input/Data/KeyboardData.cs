﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace RookV2.Input.Data
{
	public class KeyboardData : InputData
	{
		public KeyboardData(List<Keys> keysDown, List<Keys> keysPressedThisFrame, List<Keys> keysReleasedThisFrame) :
			base(InputTypes.Keyboard)
		{
			KeysDown = keysDown;
			KeysPressedThisFrame = keysPressedThisFrame;
			KeysReleasedThisFrame = keysReleasedThisFrame;
		}

		public List<Keys> KeysDown { get; }
		public List<Keys> KeysPressedThisFrame { get; }
		public List<Keys> KeysReleasedThisFrame { get; }

		public override bool Query(object data, ClickStates clickState)
		{
			Keys key = (Keys)data;

			switch (clickState)
			{
				case ClickStates.Held: return KeysDown.Contains(key);
				case ClickStates.Released: return !KeysDown.Contains(key);
				case ClickStates.PressedThisFrame: return KeysPressedThisFrame.Contains(key);
				case ClickStates.ReleasedThisFrame: return KeysReleasedThisFrame.Contains(key);
			}

			return false;
		}
	}
}
