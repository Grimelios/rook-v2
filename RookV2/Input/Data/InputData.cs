﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Input.Data
{
	public enum InputTypes
	{
		Keyboard,
		Mouse,
		Xbox360
	}

	public abstract class InputData
	{
		protected InputData(InputTypes inputType)
		{
			InputType = inputType;
		}

		public InputTypes InputType { get; }

		public abstract bool Query(object data, ClickStates clickState);
	}
}
