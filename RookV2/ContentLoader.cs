﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RookV2
{
	public static class ContentLoader
	{
		public static ContentManager Content { get; set; }

		public static SpriteFont LoadFont(string filename)
		{
			return Content.Load<SpriteFont>("Fonts/" + filename);
		}

		public static Texture2D LoadTexture(string filename)
		{
			return Content.Load<Texture2D>("Textures/" + filename);
		}
	}
}
