﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RookV2.Interfaces;

namespace RookV2.Core
{
	public abstract class Component2D : IPositionable, IRotatable, IScalable, IColorable, IDynamic, IRenderable
	{
		protected Component2D()
		{
			Color = Color.White;
			Scale = 1;
		}

		public virtual Vector2 Position { get; set; }

		public virtual float Rotation { get; set; }
		public virtual float Scale { get; set; }

		public bool FlipHorizontal { get; set; }
		public bool FlipVertical { get; set; }

		public SpriteEffects Effects
		{
			get
			{
				SpriteEffects effects = SpriteEffects.None;

				if (FlipHorizontal)
				{
					effects |= SpriteEffects.FlipHorizontally;
				}

				if (FlipVertical)
				{
					effects |= SpriteEffects.FlipVertically;
				}

				return effects;
			}
		}

		public Color Color { get; set; }

		public virtual void Update(float dt)
		{
		}

		public virtual void Draw(SuperBatch sb)
		{
		}
	}
}
