﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Data
{
	public enum FieldTypes
	{
		Integer,
		Float,
		String,
		Color,
		Enumeration
	}

	public class DataField
	{
		public DataField(string name, FieldTypes type)
		{
			Name = name;
			Type = type;
		}

		public string Name { get; }

		public FieldTypes Type { get; }
	}
}
