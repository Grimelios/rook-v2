﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Data
{
	public class EnumeratedDataField : DataField
	{
		public EnumeratedDataField(string name, Type enumerationType) : base(name, FieldTypes.Enumeration)
		{
			EnumerationType = enumerationType;
		}

		public Type EnumerationType { get; }
	}
}
