﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using RookV2.Interfaces;
using RookV2.Interfaces.Control;
using RookV2.Physics;

namespace RookV2.Controllers
{
	public class AirController : IDynamic
	{
		private IAirEnabled parent;

		private bool accelerating;
		private bool decelerating;

		public AirController(IAirEnabled parent)
		{
			this.parent = parent;
		}

		public bool Accelerating
		{
			set
			{
				accelerating = value;
				decelerating = !value && parent.Body.LinearVelocity.X != 0;
			}
		}

		// Unlike the terrain controller, the air controller only needs to be updated when active.
		public bool Active => accelerating || decelerating;

		// This value should always be either -1, 0, or 1.
		public int Direction { get; set; }

		public void Update(float dt)
		{
			Body body = parent.Body;

			float force;

			if (accelerating)
			{
				body.Accelerating = true;
				force = PhysicsConvert.ToMeters(parent.Acceleration) * Direction;
			}
			// By design, this function is only called when the controller is active (either accelerating or deceleratin). As such, the
			// controller must be decelerating here.
			else
			{
				body.Accelerating = false;
				force = PhysicsConvert.ToMeters(parent.Deceleration) * -Math.Sign(body.LinearVelocity.X);
			}

			body.ApplyRawForce(new Vector2(force, 0));
		}
	}
}
