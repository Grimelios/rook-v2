﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using RookV2.Interfaces;
using RookV2.Interfaces.Control;
using RookV2.Physics;
using RookV2.Physics.Shapes;

namespace RookV2.Controllers
{
	public class RunController : IDynamic
	{
		private IRunEnabled parent;

		private Edge edge;

		private bool accelerating;
		private bool decelerating;
		private bool againstWall;
		private bool sliding;

		private float edgeProgression;
		private float currentSpeed;

		private int direction;
		private int wallDirection;

		public RunController(IRunEnabled parent)
		{
			this.parent = parent;
		}

		public bool Accelerating
		{
			set
			{
				accelerating = value;
				decelerating = !value && currentSpeed != 0;
			}
		}

		// This value should always be either -1, 0, or 1.
		public int Direction
		{
			set
			{
				direction = value;

				// From the perspective of the controller, the parent must move away from the wall in order to unstick from the wall.
				if (againstWall && value + wallDirection == 0)
				{
					againstWall = false;
					wallDirection = 0;
				}
			}
		}

		public void SignalLanding(Edge edge, Vector2 position, float speed)
		{
			this.edge = edge;

			// When the parent lands, raw horizontal speed is transformed into sloped speed.
			currentSpeed = speed / Math.Abs((float)Math.Cos(edge.Angle));
			edgeProgression = (position.X - edge.Start.X) / edge.Width * edge.Length;
			parent.EdgePosition = Vector2.Lerp(edge.Start, edge.End, edgeProgression / edge.Length);
		}

		public void Update(float dt)
		{
			if (againstWall)
			{
				return;
			}

			// Accelerating and decelerating are mutually exclusive.
			if (accelerating)
			{
				float maxSpeed = parent.MaxSpeed;

				// Speed can temporarily be above maximum speed (in the direction of the edge) if the parent lands while moving quickly.
				if (Math.Abs(currentSpeed) > maxSpeed)
				{
					int sign = Math.Sign(currentSpeed);

					currentSpeed -= parent.Deceleration * sign * dt;
					currentSpeed = Math.Abs(currentSpeed) < maxSpeed ? maxSpeed * sign : currentSpeed;
				}
				else
				{
					currentSpeed += parent.Acceleration * direction * dt;
					currentSpeed = MathHelper.Clamp(currentSpeed, -maxSpeed, maxSpeed);
				}
			}
			else if (decelerating)
			{
				int sign = Math.Sign(currentSpeed);

				currentSpeed -= parent.Deceleration * sign * dt;

				if (sign != Math.Sign(currentSpeed))
				{
					currentSpeed = 0;
					decelerating = false;
				}
			}

			edgeProgression += currentSpeed * dt;

			if (CheckEdgeTransition())
			{
				float slope = edge.Slope;
				float absoluteSlope = Math.Abs(slope);

				// The current edge is updated in CheckEdgeTransition.
				if (absoluteSlope > parent.MaxSlope && absoluteSlope <= parent.MaxSlideSlope)
				{
					bool downhill = (direction == 1 && slope > 0) || (direction == -1 && slope < 0);

					if (downhill)
					{
						parent.SignalSlide();
						sliding = true;
					}
				}
			}

			parent.EdgePosition = Vector2.Lerp(edge.Start, edge.End, edgeProgression / edge.Length);
		}

		private bool CheckEdgeTransition()
		{
			if (edgeProgression > edge.Length)
			{
				edgeProgression -= edge.Length;
				edge = edge.Next;

				return true;
			}
			else if (edgeProgression < 0)
			{
				edge = edge.Previous;
				edgeProgression += edge.Length;

				return true;
			}

			return false;
		}
	}
}
