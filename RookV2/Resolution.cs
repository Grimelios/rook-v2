﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2
{
	public static class Resolution
	{
		private const int DefaultWindowWidth = 1280;
		private const int DefaultWindowHeight = 720;

		private const float TargetAspectRatio = 16f / 9;

		public const int Width = 1280;
		public const int Height = 720;

		public static int WindowWidth { get; set; } = DefaultWindowWidth;
		public static int WindowHeight { get; set; } = DefaultWindowHeight;

		public static Vector2 Dimensions => new Vector2(Width, Height);
		public static Vector2 Scaling => new Vector2((float)WindowWidth / Width, (float)WindowHeight / Height);

		public static Rectangle DisplayRectangle { get; private set; }

		public static void ResizeWindow(int width, int height)
		{
			WindowWidth = width;
			WindowHeight = height;

			float aspectRatio = (float)width / height;

			int displayWidth;
			int displayHeight;
			int x = 0;
			int y = 0;

			// The display area is scaled to fit as much of the window as possible while still maintaining the target aspect ratio.
			if (aspectRatio == TargetAspectRatio)
			{
				displayWidth = width;
				displayHeight = height;
			}
			else if (aspectRatio < TargetAspectRatio)
			{
				displayWidth = width;
				displayHeight = (int)(displayWidth / TargetAspectRatio);
				y = (height - displayHeight) / 2;
			}
			else
			{
				displayHeight = height;
				displayWidth = (int)(displayHeight * TargetAspectRatio);
				x = (width - displayWidth) / 2;
			}

			DisplayRectangle = new Rectangle(x, y, displayWidth, displayHeight);

			// When a window resize message is received, window dimensions can be retrieved statically from this class.
			Messaging.Send(MessageTypes.Window, null);
		}
	}
}
