﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Input.Data;

namespace RookV2
{
	using MessageAction = Action<object, float>;

	public enum MessageTypes
	{
		Keyboard,
		Mouse,
		Xbox360,
		Input,
		Window,
		Debug,
		Exit
	}

	public static class Messaging
	{
		private static List<MessageAction>[] receivers = new List<MessageAction>[Functions.EnumCount<MessageTypes>()];

		public static void Subscribe(MessageTypes messageType, MessageAction action)
		{
			VerifyList(messageType).Add(action);
		}

		public static void Send(MessageTypes messageType, object data, float dt = 0)
		{
			VerifyList(messageType).ForEach(a => a(data, dt));
		}

		private static List<MessageAction> VerifyList(MessageTypes messageType)
		{
			int index = (int)messageType;

			return receivers[index] ?? (receivers[index] = new List<MessageAction>());
		}
	}
}
