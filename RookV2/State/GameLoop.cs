﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Interfaces;

namespace RookV2.State
{
	public abstract class GameLoop : IDynamic, IRenderable
	{
		public abstract void Initialize(Camera camera);
		public abstract void Update(float dt);
		public abstract void Draw(SuperBatch sb);
	}
}
