﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using RookV2.Combat;
using RookV2.Entities;
using RookV2.Entities.Core;
using RookV2.Entities.Weapons;
using RookV2.Json;
using RookV2.Physics;
using RookV2.Physics.Shapes;
using RookV2.UI;

namespace RookV2.State
{
	public class GameplayLoop : GameLoop
	{
		private Scene scene;
		private PhysicsAccumulator accumulator;
		private PhysicsVisualizer visualizer;
		private CombatWorld combatWorld;

		public override void Initialize(Camera camera)
		{
			World world = new World(new Vector2(0, PhysicsConstants.Gravity));

			PhysicsFactory.World = world;

			accumulator = new PhysicsAccumulator(world);
			visualizer = new PhysicsVisualizer(world);
			combatWorld = new CombatWorld();

			CombatFactory.Initialize(combatWorld);

			Player player = new Player
			{
				Position = new Vector2(-550, -150)
			};

			player.Unlock(PlayerSkills.Run);
			player.Unlock(PlayerSkills.Jump);
			player.Unlock(PlayerSkills.DoubleJump);
			player.Unlock(PlayerSkills.Attack);
			player.Equip(new ToySword());

			scene = new Scene();
			scene.Add(0, player);

			LoadPlaygroundEdges();
		}

		private void LoadPlaygroundEdges()
		{
			EdgeCollection[] collections = JsonUtilities.Deserialize<EdgeCollection[]>("Playground.json");

			foreach (EdgeCollection collection in collections)
			{
				Body body = PhysicsFactory.CreateBody(null);

				foreach (Edge edge in collection.Edges)
				{
					PhysicsFactory.AttachEdge(body, edge, Units.Pixels);
				}
			}
		}

		public override void Update(float dt)
		{
			accumulator.Update(dt);
			scene.Update(dt);
			combatWorld.Update(dt);
		}

		public static Vector2 LandingPosition { get; set; }

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.World);
			scene.Draw(sb);
			visualizer.Draw(sb);
			sb.End();
		}
	}
}
