﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Editing;
using RookV2.Entities.Core;

namespace RookV2.State
{
	public class EditorLoop : GameLoop
	{
		private Scene scene;
		private Editor editor;

		public override void Initialize(Camera camera)
		{
			scene = new Scene();
			editor = new Editor(camera)
			{
				Scene = scene
			};
		}

		public override void Update(float dt)
		{
			scene.Update(dt);
			editor.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.World);
			scene.Draw(sb);
			editor.Draw(sb);
			sb.End();
		}
	}
}
