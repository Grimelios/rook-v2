﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2
{
	public static class Functions
	{
		public static int EnumCount<T>()
		{
			return Enum.GetValues(typeof(T)).Length;
		}

		public static float ComputeAngle(Vector2 vector)
		{
			return ComputeAngle(Vector2.Zero, vector);
		}

		public static float ComputeAngle(Vector2 start, Vector2 end)
		{
			float dX = end.X - start.X;
			float dY = end.Y - start.Y;

			return (float)Math.Atan2(dY, dX);
		}

		public static Vector2 ComputeDirection(float angle)
		{
			float x = (float)Math.Cos(angle);
			float y = (float)Math.Sin(angle);

			return new Vector2(x, y);
		}

		public static Vector2 ComputeOrigin(Alignments alignment, Vector2 dimensions)
		{
			return ComputeOrigin(alignment, (int)dimensions.X, (int)dimensions.Y);
		}

		public static Vector2 ComputeOrigin(Alignments alignment, int width, int height)
		{
			bool left = (alignment & Alignments.Left) > 0;
			bool right = (alignment & Alignments.Right) > 0;
			bool top = (alignment & Alignments.Top) > 0;
			bool bottom = (alignment & Alignments.Bottom) > 0;

			Vector2 origin;
			origin.X = left ? 0 : (right ? width : width / 2);
			origin.Y = top ? 0 : (bottom ? height : height / 2);

			// Since width and height are integers, the origin is guaranteed to contain integer values.
			return origin;
		}
	}
}
