﻿using System;

namespace RookV2
{
    public static class Program
    {
        public static void Main()
        {
	        using (MainGame game = new MainGame())
	        {
		        game.Run();
	        }
        }
    }
}
