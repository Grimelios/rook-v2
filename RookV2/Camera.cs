﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RookV2.Core;
using RookV2.Entities.Core;
using RookV2.Interfaces;

namespace RookV2
{
	public class Camera : IPositionable, IRotatable, IDynamic
	{
		public Camera()
		{
			Origin = Resolution.Dimensions / 2;
			Transform = Matrix.Identity;
			VisibleArea = new Bounds(Resolution.WindowWidth, Resolution.WindowHeight);
		}

		public Vector2 Position { get; set; }
		public Vector2 Origin { get; set; }
		public Bounds VisibleArea { get; }

		public float Rotation { get; set; }

		public Matrix Transform { get; private set; }
		public Entity Target { get; set; }

		public void Update(float dt)
		{
			if (Target != null)
			{
				Position = Target.Position.Integerize();
			}

			VisibleArea.Position = (Position - Origin).ToPoint();
			Transform =
				Matrix.CreateTranslation(new Vector3(Origin - Position, 0)) *
				Matrix.CreateRotationZ(Rotation);

			Messaging.Send(MessageTypes.Debug, $"{VisibleArea.X}, {VisibleArea.Y}, {VisibleArea.Width}, {VisibleArea.Height}");
		}
	}
}
