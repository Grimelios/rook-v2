﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2.Physics.Shapes
{
	public class Edge
	{
		private Vector2 start;
		private Vector2 end;

		public Edge(Vector2 start, Vector2 end)
		{
			Start = start;
			End = end;
		}

		public Vector2 Start
		{
			get { return start; }
			set
			{
				start = value;
				Length = Vector2.Distance(start, end);
			}
		}

		public Vector2 End
		{
			get { return end; }
			set
			{
				end = value;
				Length = Vector2.Distance(start, end);
			}
		}

		public float Length { get; private set; }
		public float Width => End.X - Start.X;
		public float Height => End.Y - Start.Y;
		public float Slope => end.X == start.X ? float.NaN : (end.Y - start.Y) / (end.X - start.X);
		public float Angle => Functions.ComputeAngle(start, end);

		public Edge Next { get; set; }
		public Edge Previous { get; set; }
	}
}
