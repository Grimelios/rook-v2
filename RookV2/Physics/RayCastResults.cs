﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RookV2.Entities.Core;

namespace RookV2.Physics
{
	public class RayCastResults
	{
		public RayCastResults(Entity entity, Vector2 position)
		{
			Entity = entity;
			Position = position;
		}

		public Entity Entity { get; }
		public Vector2 Position { get; }
	}
}
