﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2.Physics
{
	public static class PhysicsConvert
	{
		public static float ToPixels(float value)
		{
			return value * PhysicsConstants.PixelsPerMeter;
		}

		public static float ToMeters(float value)
		{
			return value / PhysicsConstants.PixelsPerMeter;
		}

		public static Vector2 ToPixels(Vector2 value)
		{
			return value * PhysicsConstants.PixelsPerMeter;
		}

		public static Vector2 ToMeters(Vector2 value)
		{
			return value / PhysicsConstants.PixelsPerMeter;
		}
	}
}
