﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using RookV2.Entities.Core;
using RookV2.Physics.Shapes;

namespace RookV2.Physics
{
	public enum Units
	{
		Pixels,
		Meters
	}

	public enum PhysicsGroups
	{
		World = Category.Cat1,
		Player = Category.Cat2
	}

	public static class PhysicsFactory
	{
		public static World World { get; set; }

		public static Body CreateBody(Entity entity)
		{
			Body body = BodyFactory.CreateBody(World);
			body.UserData = entity;

			return body;
		}

		public static Body CreateCircle(float radius, Units units, BodyType bodyType, Entity entity)
		{
			return CreateCircle(radius, Vector2.Zero, units, bodyType, entity);
		}

		public static Body CreateCircle(float radius, Vector2 position, Units units, BodyType bodyType, Entity entity)
		{
			if (units == Units.Pixels)
			{
				radius = PhysicsConvert.ToMeters(radius);
				position = PhysicsConvert.ToMeters(position);
			}

			Body body = BodyFactory.CreateCircle(World, radius, 1, position);
			body.BodyType = bodyType;
			body.UserData = entity;

			return body;
		}

		public static Body CreateRectangle(float width, float height, Units units, BodyType bodyType, Entity entity)
		{
			return CreateRectangle(width, height, Vector2.Zero, units, bodyType, entity);
		}

		public static Body CreateRectangle(float width, float height, Vector2 position, Units units, BodyType bodyType, Entity entity)
		{
			if (units == Units.Pixels)
			{
				width = PhysicsConvert.ToMeters(width);
				height = PhysicsConvert.ToMeters(height);
				position = PhysicsConvert.ToMeters(position);
			}

			Body body = BodyFactory.CreateRectangle(World, width, height, 1, position);
			body.BodyType = bodyType;
			body.UserData = entity;

			return body;
		}

		public static Body CreateDiamond(float width, float height, Units units, Entity entity)
		{
			Vector2[] points =
			{
				new Vector2(-width / 2, 0),
				new Vector2(0, -height / 2),
				new Vector2(width / 2, 0),
				new Vector2(0, height / 2),
			};

			if (units == Units.Pixels)
			{
				for (int i = 0; i < points.Length; i++)
				{
					points[i] = PhysicsConvert.ToMeters(points[i]);
				}
			}
			
			Body body = BodyFactory.CreatePolygon(World, new Vertices(points), 1);
			body.BodyType = BodyType.Dynamic;
			body.UserData = entity;

			return body;
		}

		public static Fixture AttachEdge(Body body, Edge edge, Units units)
		{
			Vector2 start = edge.Start;
			Vector2 end = edge.End;

			if (units == Units.Pixels)
			{
				start = PhysicsConvert.ToMeters(start);
				end = PhysicsConvert.ToMeters(end);
			}

			Fixture fixture = FixtureFactory.AttachEdge(start, end, body);
			fixture.UserData = edge;

			return fixture;
		}
	}
}
