﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace RookV2
{
	public static class Extensions
	{
		public static Vector2 Integerize(this Vector2 value)
		{
			return new Vector2((int)value.X, (int)value.Y);
		}
	}
}
