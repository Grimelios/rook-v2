﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Core;
using RookV2.Entities.Core;

namespace RookV2.Combat
{
	public class ArcAttack : AttackShape
	{
		public ArcAttack(CombatWorld world, Entity parent) : base(world, parent)
		{
		}

		public int Radius { get; set; }

		public float Spread { get; set; }

		public override bool Intersects(Bounds bounds)
		{
			return true;
		}
	}
}
