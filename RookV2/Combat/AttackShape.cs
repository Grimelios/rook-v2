﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using RookV2.Core;
using RookV2.Entities.Core;
using RookV2.Interfaces;

namespace RookV2.Combat
{
	public abstract class AttackShape : IPositionable, IBoundable, IDynamic
	{
		private Timer timer;
		private List<ITargetable> targetsHit;

		protected AttackShape(CombatWorld world, Entity parent)
		{
			Parent = parent;
			Bounds = new Bounds();
			targetsHit = new List<ITargetable>();

			world.Shapes.Add(this);
		}

		public Vector2 Position { get; set; }
		public Bounds Bounds { get; }
		public Entity Parent { get; }

		public int Damage { get; set; }
		public int Lifetime
		{
			// Lifetime can be left unset to make the attack shape last indefinitely.
			set { timer = new Timer(value, time => { Complete = true; }); }
		}

		public bool Complete { get; private set; }

		public abstract bool Intersects(Bounds bounds);

		public virtual void Update(float dt)
		{
			timer.Update(dt);
		}
	}
}
