﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Interfaces;

namespace RookV2.Combat
{
	public class CombatWorld : IDynamic
	{
		public List<AttackShape> Shapes { get; } = new List<AttackShape>();

		public void Update(float dt)
		{
			Shapes.ForEach(s => s.Update(dt));

			for (int i = Shapes.Count - 1; i >= 0; i--)
			{
				if (Shapes[i].Complete)
				{
					Shapes.RemoveAt(i);
				}
			}
		}
	}
}
