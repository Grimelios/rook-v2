﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Entities.Core;

namespace RookV2.Combat
{
	public static class CombatFactory
	{
		private static CombatWorld world;

		public static void Initialize(CombatWorld w)
		{
			world = w;
		}

		public static AttackShape CreateArc(float spread, int radius, int damage, int lifetime, Entity parent)
		{
			return new ArcAttack(world, parent)
			{
				Spread = spread,
				Radius = radius,
				Damage = damage,
				Lifetime = lifetime
			};
		}
	}
}
