﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RookV2.Core;
using RookV2.Entities.Core;
using RookV2.Json;
using RookV2.Physics;
using RookV2.Physics.Shapes;

namespace RookV2.Entities
{
	public class Tilemap : Entity
	{
		private Texture2D tilesheet;
		private Body[] bodies;

		private int tilesPerRow;
		
		public Tilemap(string jsonFile) : this(JsonUtilities.Deserialize<TilemapData>("Tilemaps/" + jsonFile))
		{
		}
		
		public Tilemap(TilemapData data) : base(EntityTypes.World)
		{
			Width = data.Width;
			Height = data.Height;
			Tiles = data.Tiles;
			TileSize = data.TileSize;
			tilesheet = ContentLoader.LoadTexture("Tilesheets/" + data.Tilesheet);
			tilesPerRow = tilesheet.Width / TileSize;

			if (data.Edges != null)
			{
				CreateBodies(data.Edges);
			}
		}
		
		public Tilemap(Bounds bounds, Texture2D tilesheet, int tileSize, int tilesPerRow) : base(EntityTypes.World)
		{
			this.tilesheet = tilesheet;
			this.tilesPerRow = tilesPerRow;

			Bounds = bounds;
			TileSize = tileSize;
			Position = Bounds.Position.ToVector2();

			int width = Width;
			int height = Height;

			// When creating a tilemap in this way, tiles are filled in manually in the editor.
			Tiles = new int[width, height];

			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Tiles[j, i] = -1;
				}
			}
		}
		
		public override Vector2 Position
		{
			set
			{
				if (bodies != null)
				{
					Vector2 convertedPosition = PhysicsConvert.ToMeters(value);

					foreach (Body body in bodies)
					{
						body.Position = convertedPosition;
					}
				}

				base.Position = value;
			}
		}
		
		public Point Coordinates => new Point((int)Position.X / TileSize, (int)Position.Y / TileSize);
		
		public int Width
		{
			get { return Bounds.Width / TileSize; }
			set { Bounds.Width = value * TileSize; }
		}
		
		public int Height
		{
			get { return Bounds.Height / TileSize; }
			set { Bounds.Height = value * TileSize; }
		}

		public int TileSize { get; set; }
		public int[,] Tiles { get; set; }
		
		private void CreateBodies(EdgeCollection[] edges)
		{
			bodies = new Body[edges.Length];

			for (int i = 0; i < edges.Length; i++)
			{
				Body body = PhysicsFactory.CreateBody(this);
				Edge[] edgeArray = edges[i].Edges;

				foreach (Edge edge in edgeArray)
				{
					PhysicsFactory.AttachEdge(body, edge, Units.Meters);
				}

				body.CollisionCategories = (Category)PhysicsGroups.World;
				bodies[i] = body;
			}
		}
		
		public override void Draw(SuperBatch sb)
		{
			Rectangle sourceRect = new Rectangle(0, 0, TileSize, TileSize);

			for (int i = 0; i < Height; i++)
			{
				for (int j = 0; j < Width; j++)
				{
					int tileValue = Tiles[j, i];

					// -1 represents a blank tile.
					if (tileValue > -1)
					{
						sourceRect.X = tileValue % tilesPerRow * TileSize;
						sourceRect.Y = tileValue / tilesPerRow * TileSize;

						sb.Draw(tilesheet, Position + new Vector2(j, i) * TileSize, sourceRect);
					}
				}
			}
		}
	}
}
