﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using RookV2.Controllers;
using RookV2.Core;
using RookV2.Entities.Core;
using RookV2.Entities.Weapons;
using RookV2.Input.Data;
using RookV2.Interfaces.Control;
using RookV2.Json;
using RookV2.Physics;
using RookV2.Physics.Shapes;
using RookV2.State;

namespace RookV2.Entities
{
	public enum PlayerSkills
	{
		Run,
		Jump,
		DoubleJump,
		Attack
	}

	public class Player : LivingEntity, IRunEnabled, IAirEnabled
	{
		private const int RunIndex = (int)PlayerSkills.Run;
		private const int JumpIndex = (int)PlayerSkills.Jump;
		private const int DoubleJumpIndex = (int)PlayerSkills.DoubleJump;
		private const int AttackIndex = (int)PlayerSkills.Attack;

		private PlayerData playerData;
		private PlayerControls controls;
		private RunController runController;
		private AirController airController;
		private Body terrainBody;

		private bool[] skillsEnabled;
		private bool[] skillsUnlocked;
		private bool jumpActive;
		private bool jumpDecelerating;
		private bool sliding;

		private int jumpsRemaining;

		private ToySword toySword;

		public Player() : base(EntityTypes.Player)
		{
			playerData = JsonUtilities.Deserialize<PlayerData>("Player.json");
			runController = new RunController(this);
			airController = new AirController(this);
			controls = new PlayerControls();
			Bounds = new Bounds(playerData.HitboxWidth, playerData.HitboxHeight);

			skillsEnabled = new bool[Functions.EnumCount<PlayerSkills>()];
			skillsUnlocked = new bool[skillsEnabled.Length];

			int width = playerData.HitboxWidth;
			int height = playerData.HitboxHeight;
			
			Body = PhysicsFactory.CreateDiamond(width, height, Units.Pixels, this);
			Body.Friction = 0;
			Body.FixedRotation = true;
			Body.CollisionCategories = (Category)PhysicsGroups.Player;
			Body.CollidesWith = (Category)PhysicsGroups.World;
			Body.ManuallyControlled = true;
			Body.MaximumSpeed = PhysicsConvert.ToMeters(playerData.MaxSpeed);

			Components.Add(new Sprite("Player"));

			Messaging.Subscribe(MessageTypes.Input, (data, dt) => ProcessInput((AggregateInputData)data, dt));
		}

		public Vector2 EdgePosition
		{
			get { return Position + new Vector2(0, Bounds.Height / 2); }
			set { Position = value - new Vector2(0, Bounds.Height / 2); }
		}

		public float Acceleration => playerData.Acceleration;
		public float Deceleration => playerData.Deceleration;
		public float SlideAcceleration => playerData.SlideAcceleration;
		public float SlideDeceleration => playerData.SlideDeceleration;
		public float MaxSpeed => playerData.MaxSpeed;
		public float MaxSlope => playerData.MaxSlope;
		public float MaxSlideSlope => playerData.MaxSlideSlope;
		public float MaxSlideSpeed => playerData.MaxSlideSpeed;

		protected override bool OnCollision(Body body, Entity entity, object fixtureData, Vector2 position, Vector2 normal)
		{
			Edge edge = fixtureData as Edge;

			if (!OnGround)
			{
				// To land on a surface, the player must be moving down relative to the world entity. This could also apply to a platform
				// moving up faster than the player, even if the player's absolute velocity is upward.
				float deltaY = Velocity.Y - (entity == null ? 0 : entity.Velocity.Y);

				if (deltaY < 0 || Math.Abs(edge.Slope) > playerData.MaxSlope)
				{
					return true;
				}

				OnLanding(edge, position, body);
			}

			return true;
		}

		private void OnLanding(Edge edge, Vector2 landingPosition, Body terrainBody)
		{
			this.terrainBody = terrainBody;

			bool jumpUnlocked = skillsUnlocked[JumpIndex];
			bool doubleJumpUnlocked = skillsUnlocked[DoubleJumpIndex];

			OnGround = true;
			skillsEnabled[JumpIndex] = jumpUnlocked;
			skillsEnabled[DoubleJumpIndex] = doubleJumpUnlocked;
			runController.SignalLanding(edge, landingPosition, PhysicsConvert.ToPixels(Body.LinearVelocity.X));
			Body.IgnoreGravity = true;
			Body.IgnoreCollisionWith(terrainBody);

			// If double jump is unlocked, normal jump is also assumed to be unlocked (although if normal jump was artifically disabled,
			// you'd only be able to double jump after leaving the ground without jumping).
			if (doubleJumpUnlocked)
			{
				jumpsRemaining = 2;
			}
			else if (jumpUnlocked)
			{
				jumpsRemaining = 1;
			}
		}

		private void ProcessInput(AggregateInputData data, float dt)
		{
			if (skillsUnlocked[RunIndex])
			{
				ProcessRun(data, dt);
			}

			if (skillsUnlocked[JumpIndex])
			{
				ProcessJump(data);
			}

			// If attacking is unlocked, the player is assumed to always have a sword available.
			if (skillsUnlocked[AttackIndex])
			{
				ProcessAttack(data);
			}
		}

		private void ProcessRun(AggregateInputData data, float dt)
		{
			bool leftHeld = data.Query(controls.RunLeft, ClickStates.Held);
			bool rightHeld = data.Query(controls.RunRight, ClickStates.Held);

			if (OnGround)
			{
				if (leftHeld ^ rightHeld)
				{
					runController.Accelerating = true;
					runController.Direction = leftHeld ? -1 : 1;
				}
				else
				{
					runController.Accelerating = false;
					runController.Direction = 0;
				}

				SuppressBodyPositioning = true;
				runController.Update(dt);
				Velocity = (Position - PreviousPosition) / dt;
				SuppressBodyPositioning = false;
			}
			else
			{
				if (leftHeld ^ rightHeld)
				{
					airController.Accelerating = true;
					airController.Direction = leftHeld ? -1 : 1;
				}
				else
				{
					airController.Accelerating = false;
					airController.Direction = 0;
				}

				if (airController.Active)
				{
					airController.Update(dt);
				}
			}
		}

		private void ProcessJump(AggregateInputData data)
		{
			if (jumpActive && data.Query(controls.Jump, ClickStates.ReleasedThisFrame))
			{
				ReleaseJump();
			}
			// If a jump is available, either jump or double jump (or both) must be unlocked and enabled.
			else if (jumpsRemaining > 0 && data.Query(controls.Jump, ClickStates.PressedThisFrame))
			{
				if (skillsEnabled[JumpIndex])
				{
					TriggerJump();
				}
				else
				{
					TriggerDoubleJump();
				}
			}
		}

		private void ProcessAttack(AggregateInputData data)
		{
			if (data.Query(controls.Attack, ClickStates.PressedThisFrame))
			{
				toySword.TriggerAttack();
			}
		}

		private void TriggerJump()
		{
			Body.LinearVelocity = new Vector2(Body.LinearVelocity.X, -playerData.JumpSpeed);
			jumpsRemaining--;
			jumpActive = true;
			OnGround = false;
			skillsEnabled[JumpIndex] = false;
			Body.IgnoreGravity = false;
			Body.RestoreCollisionWith(terrainBody);
			terrainBody = null;
		}

		private void TriggerDoubleJump()
		{
			Body.LinearVelocity = new Vector2(Body.LinearVelocity.X, -playerData.DoubleJumpSpeed);
			jumpsRemaining--;
			jumpActive = true;

			// Rook will likely only be designed with single and double jumping, but using a counter variable allows the game to be
			// easily modified in the future to allow additional jumps. If additional jumps are enabled, the double jump skill (if
			// unlocked) would remain enabled until all jumps are used.
			skillsEnabled[DoubleJumpIndex] = jumpsRemaining == 0;
		}

		private void ReleaseJump()
		{
			if (Body.LinearVelocity.Y < -playerData.JumpSpeedLimited)
			{
				jumpDecelerating = true;
			}

			jumpActive = false;
		}

		public void SignalSlide()
		{
			sliding = true;
		}

		public void SignalUnslide()
		{
			sliding = false;
		}

		public void SignalPerch()
		{
		}

		public void SignalFall()
		{
		}

		public void Equip(ToySword sword)
		{
			toySword = sword;
			toySword.Parent = this;
		}

		public void Unlock(PlayerSkills skill)
		{
			int index = (int)skill;

			skillsUnlocked[index] = true;
			skillsEnabled[index] = CheckSkillEnablingOnUnlock(skill);
		}

		private bool CheckSkillEnablingOnUnlock(PlayerSkills skill)
		{
			switch (skill)
			{
				case PlayerSkills.Jump:
				case PlayerSkills.DoubleJump:
					jumpsRemaining = skill == PlayerSkills.Jump ? 1 : 2;

					return OnGround;
			}

			return true;
		}
		
		public override void Update(float dt)
		{
			// The run controller will only be active on the ground, which means the player cannot be jumping.
			if (jumpDecelerating)
			{
				if (Body.LinearVelocity.Y >= -playerData.JumpSpeedLimited)
				{
					jumpDecelerating = false;
				}
				else
				{
					Body.ApplyRawForce(new Vector2(0, playerData.JumpDeceleration));
				}
			}

			Messaging.Send(MessageTypes.Debug, "On ground: " + OnGround);
			Messaging.Send(MessageTypes.Debug, "Jumps remaining: " + jumpsRemaining);
			Messaging.Send(MessageTypes.Debug, "Sliding: " + sliding);

			base.Update(dt);
		}
	}
}
