﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using RookV2.Combat;
using RookV2.Entities.Core;
using RookV2.Interfaces;
using RookV2.Json;

namespace RookV2.Entities.Weapons
{
	public class ToySword : Entity
	{
		private AttackShape attackShape;

		private int damage;
		private int lifetime;
		private int radius;

		private float spread;

		public ToySword()
		{
			ToySwordData data = JsonUtilities.Deserialize<ToySwordData>("ToySword.json");
			damage = data.Damage;
			lifetime = data.Lifetime;
			radius = data.Radius;
			spread = data.Spread;
		}

		public Player Parent { get; set; }

		public void TriggerAttack()
		{
			attackShape = CombatFactory.CreateArc(spread, radius, damage, lifetime, Parent);
		}

		private class ToySwordData
		{
			[JsonProperty]
			public int Damage { get; set; }

			[JsonProperty]
			public int Lifetime { get; set; }

			[JsonProperty]
			public int Radius { get; set; }

			[JsonProperty]
			public float Spread { get; set; }
		}
	}
}
