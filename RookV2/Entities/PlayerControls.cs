﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using RookV2.Input.Data;

namespace RookV2.Entities
{
	public class PlayerControls
	{
		public PlayerControls()
		{
			Jump = new List<InputBind>();
			RunLeft = new List<InputBind>();
			RunRight = new List<InputBind>();
			Attack = new List<InputBind>();

			Jump.Add(new InputBind(InputTypes.Keyboard, Keys.Space));
			RunLeft.Add(new InputBind(InputTypes.Keyboard, Keys.A));
			RunRight.Add(new InputBind(InputTypes.Keyboard, Keys.D));
			Attack.Add(new InputBind(InputTypes.Mouse, MouseButtons.Left));
		}

		public List<InputBind> Jump { get; }
		public List<InputBind> RunLeft { get; }
		public List<InputBind> RunRight { get; }
		public List<InputBind> Attack { get; }
	}
}
