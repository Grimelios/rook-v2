﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RookV2.Physics;

namespace RookV2.Entities
{
	public class PlayerData
	{
		private float jumpSpeed;
		private float doubleJumpSpeed;
		private float jumpSpeedLimited;
		private float jumpDecleration;

		[JsonProperty]
		public int HitboxWidth { get; set; }

		[JsonProperty]
		public int HitboxHeight { get; set; }

		[JsonProperty]
		public int Acceleration { get; set; }

		[JsonProperty]
		public int Deceleration { get; set; }

		[JsonProperty]
		public int SlideAcceleration { get; set; }

		[JsonProperty]
		public int SlideDeceleration { get; set; }

		[JsonProperty]
		public int MaxSpeed { get; set; }

		[JsonProperty]
		public int MaxSlideSpeed { get; set; }

		[JsonProperty]
		public float MaxSlope { get; set; }

		[JsonProperty]
		public float MaxSlideSlope { get; set; }

		[JsonProperty]
		public float JumpSpeed
		{
			get { return jumpSpeed; }
			set { jumpSpeed = PhysicsConvert.ToMeters(value); }
		}

		[JsonProperty]
		public float DoubleJumpSpeed
		{
			get { return doubleJumpSpeed; }
			set { doubleJumpSpeed = PhysicsConvert.ToMeters(value); }
		}

		[JsonProperty]
		public float JumpSpeedLimited
		{
			get { return jumpSpeedLimited; }
			set { jumpSpeedLimited = PhysicsConvert.ToMeters(value); }
		}

		[JsonProperty]
		public float JumpDeceleration
		{
			get { return jumpDecleration; }
			set { jumpDecleration = PhysicsConvert.ToMeters(value); }
		}
	}
}
