﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RookV2.Core;
using RookV2.Interfaces;

namespace RookV2.UI
{
	public class DebugDisplay : IPositionable, IRenderable
	{
		private SpriteFont font;
		private List<SpriteText> textList;

		public DebugDisplay()
		{
			font = ContentLoader.LoadFont("Debug");
			textList = new List<SpriteText>();

			Messaging.Subscribe(MessageTypes.Debug, (data, dt) => ProcessMessage((string)data));
		}

		public Vector2 Position { get; set; }

		private void ProcessMessage(string message)
		{
			textList.Add(new SpriteText(font, message)
			{
				Position = Position + new Vector2(0, Defaults.Spacing * textList.Count)
			});
		}
		
		public void Draw(SuperBatch sb)
		{
			textList.ForEach(t => t.Draw(sb));
			textList.Clear();
		}
	}
}
