﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RookV2.Json;

namespace RookV2.Animation
{
	public static class SpritesheetAnimationLoader
	{
		private static Dictionary<string, SpritesheetAnimation> cache = new Dictionary<string, SpritesheetAnimation>();

		public static SpritesheetAnimation Load(string filename)
		{
			SpritesheetAnimation animation;

			if (!cache.TryGetValue(filename, out animation))
			{
				animation = JsonUtilities.Deserialize<SpritesheetAnimation>("Animations/" + filename);
				cache.Add(filename, animation);
			}

			return animation;
		}
	}
}
