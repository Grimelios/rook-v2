﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RookV2.Core;
using RookV2.Interfaces;

namespace RookV2.Animation
{
	public class SpritesheetAnimationPlayer : Component2D
	{
		private Texture2D texture;
		private Vector2 origin;
		private Rectangle sourceRect;
		private SpritesheetAnimationStrip strip;

		private int currentFrame;
		private float frameElapsed;

		public SpritesheetAnimationPlayer(Texture2D texture, SpritesheetAnimationStrip strip, int frameY)
		{
			this.texture = texture;
			this.strip = strip;

			int width = strip.FrameWidth;
			int height = strip.FrameHeight;

			sourceRect = new Rectangle(0, frameY, width, height);
			origin = new Vector2(width, height) / 2;
		}

		public override void Update(float dt)
		{
			frameElapsed += dt * 1000;

			if (frameElapsed >= strip.FrameDuration)
			{
				currentFrame = currentFrame == strip.FrameCount - 1 ? 0 : ++currentFrame;
				sourceRect.X = currentFrame * strip.FrameWidth;
				frameElapsed -= strip.FrameDuration;
			}
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Draw(texture, Position, sourceRect, Color, Rotation, origin, Scale, Effects);
		}
	}
}
