﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RookV2.Animation
{
	public class SpritesheetAnimationStrip
	{
		public SpritesheetAnimationStrip(string name, int frameWidth, int frameHeight, int frameCount, int frameDuration, bool looped)
		{
			Name = name;
			FrameWidth = frameWidth;
			FrameHeight = frameHeight;
			FrameCount = frameCount;
			FrameDuration = frameDuration;
			Looped = looped;
		}

		public string Name { get; }

		public int FrameWidth { get; }
		public int FrameHeight { get; }
		public int FrameCount { get; }
		public int FrameDuration { get; }

		public bool Looped { get; }
	}
}
