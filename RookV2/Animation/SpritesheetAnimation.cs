﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace RookV2.Animation
{
	public class SpritesheetAnimation
	{
		private Texture2D texture;
		private SpritesheetAnimationStrip[] strips;

		private int[] frameYArray;

		public SpritesheetAnimation(string texture, SpritesheetAnimationStrip[] strips)
		{
			this.strips = strips;
			this.texture = ContentLoader.LoadTexture("Animations/" + texture);

			frameYArray = new int[strips.Length];

			int frameY = 0;

			for (int i = 0; i < strips.Length; i++)
			{
				frameYArray[i] = frameY;
				frameY += strips[i].FrameHeight;
			}
		}

		public SpritesheetAnimationPlayer Play(string name)
		{
			int index = 0;

			while (strips[index].Name != name)
			{
				index++;
			}

			return new SpritesheetAnimationPlayer(texture, strips[index], frameYArray[index]);
		}
	}
}
